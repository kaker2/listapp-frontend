import 'package:listapp/utils/api.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class Helpers {
  get(String name, context) {
    name = name.toLowerCase();

    switch (name) {
      case 'api':
        return Api();
        break;
      case 'lang':
        return AppLocalizations.of(context);
        break;
    }
  }
}
