import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:listapp/auth/services/auth_service.dart';
import 'package:listapp/common/routes/app_routes.dart';

class CustomDrawer extends StatelessWidget {
  final authService = Get.find<AuthService>();

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          ListTile(
            title: Text("Moje listy"),
            leading: Icon(Icons.list),
            onTap: () {
              Get.toNamed(AppRoutes.Lists);
            },
          ),
          ListTile(
            title: Text("Wyloguj"),
            leading: Icon(Icons.logout),
            onTap: () {
              authService.deleteData('jwt');
              Get.toNamed(AppRoutes.AuthLogin);
            },
          ),
        ],
      ),
    );
  }
}
