import 'package:http/http.dart' as http;
import 'package:listapp/utils/constants.dart' as CONST;
import 'dart:convert';

class Api {
  String api = CONST.API_V1_URL;
  get(String url) async {
    final response = await http.get(Uri.parse(api + url));
    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      throw Exception('Failed to load lists');
    }
  }

  post(url, data) async {
    // var args = {"items": json.encode(data)};
    var response = null;
    try {
      //
      var uri = Uri.parse(api + url);
      var request = await http.post(uri, body: data);
      response = {
        "statusCode": request.statusCode,
        "body": request.body,
      };
      if (request.statusCode != 200) {
        throw Exception(response);
      }
      //
    } catch (e) {
      print(e);
    }
    return response;
  }

  // post(String url, data) async {
  //   // var args = {"items": json.encode(data)};
  //   var response = null;
  //   try {
  //     print(Uri.parse(api + url));
  //     var request = await http.post(Uri.parse(api + url), body: data);
  //     print('request');
  //     print(request);

  //     response = {
  //       "statusCode": request.statusCode,
  //       "body": request.body,
  //     };
  //   } catch (e) {
  //     print(e);
  //   }
  //   return response;
  // }

  put(String url, data) async {
    Map<String, dynamic> args = {"items": json.encode(data)};

    try {
      var request = await http.put(Uri.parse(api + url), body: args);
      if (request.statusCode == 200) {
        // If the server did return a 200 OK response,
        // then parse the JSON.
        return json.decode(request.body);
      } else {
        throw Exception('Failed to load lists');
      }
    } catch (e) {
      print(e);
    }
  }

  delete(String url) async {
    try {
      var request = await http.delete(Uri.parse(api + url));
      var response = {
        "statusCode": request.statusCode,
        "body": request.body,
      };
    } catch (e) {
      print(e);
    }
  }
}
