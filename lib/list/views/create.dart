import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:listapp/common/mixins/api_call.dart';
import 'package:listapp/common/services/sqlite_service.dart';
import 'package:listapp/common/routes/app_routes.dart';

class ListCreatePage extends StatefulWidget {
  @override
  _ListCreatePageState createState() => _ListCreatePageState();
}

class _ListCreatePageState extends State<ListCreatePage> with ApiCall {
  static var _searchButton = TextEditingController();
  static var _listTitle = TextEditingController();
  final sqliteService = Get.find<SqliteService>();
  static FocusNode _searchButtonFocus;

  DateTime dateToday = DateTime(
      DateTime.now().year,
      DateTime.now().month,
      DateTime.now().day,
      DateTime.now().hour,
      DateTime.now().minute,
      DateTime.now().second);

  var products = [];

  @override
  void initState() {
    super.initState();
    this.initApi("lists");
    _searchButtonFocus = FocusNode();

    Future.delayed(Duration.zero, () async {
      _listTitle.text = "Nowa lista ";
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: new TextField(
        controller: _listTitle,
        decoration: InputDecoration(
          fillColor: Colors.white,
          filled: true,
          hintText: 'Nazwa listy',
        ),
      )),
      body: Column(mainAxisSize: MainAxisSize.min, children: [
        Row(
          children: <Widget>[
            new Flexible(
              child: new TextField(
                controller: _searchButton,
                focusNode: _searchButtonFocus,
                onSubmitted: (value) {
                  setState(() {
                    if (_searchButton.text.length > 0)
                      products.add(jsonEncode(
                          {"text": _searchButton.text, "checked": false}));
                    _searchButton.clear();
                    _searchButtonFocus.requestFocus();
                  });
                },
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(20.0),
                  suffixIcon: IconButton(
                      icon: Icon(
                        Icons.add_circle,
                      ),
                      iconSize: 24,
                      color: Colors.green,
                      tooltip: "Dodaj produkt",
                      onPressed: () {
                        setState(() {
                          if (_searchButton.text.length > 0)
                            products.add(jsonEncode({
                              "text": _searchButton.text,
                              "checked": false
                            }));
                          _searchButton.clear();
                        });
                      }),
                ),
              ),
            ),
          ],
        ),
        new Expanded(
          child: new ListView.builder(
            itemCount: products.length,
            itemBuilder: (
              BuildContext context,
              int index,
            ) {
              var product = jsonDecode(products[index]);

              return new ListTile(
                title: Text(product['text']),
                trailing: IconButton(
                    icon: Icon(Icons.close),
                    iconSize: 14,
                    color: Colors.redAccent,
                    onPressed: () {
                      setState(() {
                        products.remove(products[index]);
                      });
                    }),
              );
            },
          ),
        )
      ]),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          saveData();
        },
        tooltip: "Zapisz",
        child: Icon(Icons.save),
        backgroundColor: Colors.green,
      ),
    );
  }

  void saveData() async {
    EasyLoading.show(status: 'Dodawanie listy.');

    if (products.length <= 0) {
      EasyLoading.showToast('Dodaj przynajmniej jeden element do listy');
      return;
    }

    await this.postApi("", {
      'title': _listTitle.text,
      'items': json.encode(products)
    }).then((response) {
      print({'response': response});
      EasyLoading.showSuccess('Nowa lista została dodana.');

      _listTitle.clear();

      Get.toNamed(AppRoutes.Lists);
    });
  }

  void searchOperation(String text) {}
}
