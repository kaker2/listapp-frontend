import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:link_text/link_text.dart';
import 'package:listapp/auth/module.dart';
import 'package:listapp/utils/constants.dart' as CONST;

import 'package:listapp/common/mixins/api_call.dart';
import 'package:listapp/list/model/list.dart';
import 'package:listapp/utils/extensions.dart';

class ShareList extends StatelessWidget with ApiCall {
  Lists data;
  final _searchEmail = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  ShareList({data}) {
    this.data = data;
    this.initApi("lists-shared");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Udostępnianie :' + this.data.title),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(10.0),
          child: Column(mainAxisSize: MainAxisSize.min, children: [
            Card(
              clipBehavior: Clip.antiAlias,
              child: Column(
                children: [
                  Ink(
                      child: Form(
                    key: _formKey,
                    child: ListTile(
                      title: TextFormField(
                          controller: _searchEmail,
                          decoration: InputDecoration(
                              labelText: Module().get('lang', context).email),
                          // obscureText: true,
                          validator: (String value) {
                            var text = '';
                            if (value.isEmpty) {
                              return Module().get('lang', context).empty_field;
                            }
                            if (!value.isValidEmail()) {
                              return Module()
                                  .get('lang', context)
                                  .email_error_valid_email;
                            }
                          }),
                      trailing: IconButton(
                          icon: Icon(Icons.send),
                          iconSize: 18,
                          tooltip: "Wyślij",
                          color: Colors.black,
                          onPressed: () {
                            final FormState form = _formKey.currentState;
                            if (form.validate()) {
                              shareList();
                            } else {
                              print('Form is invalid');
                            }
                          }),
                    ),
                  )),
                  ListTile(
                      title: LinkText(this.data.share_url),
                      trailing: TextButton(
                        onPressed: () {},
                        child: Text(Module().get('lang', context).btn_send),
                      )),
                ],
              ),
            ),
          ]),
        ),
      ),
    );
  }

  shareList([value = '']) async {
    EasyLoading.show(status: "Proszę czekać");
    this.postWithoutSqlite("/share", {
      'email': (value != '') ? value : _searchEmail.text,
      'list': this.data.id,
      'shared_url': CONST.LIST_SHARED_URL + '/' + this.data.id
    }).then((response) {
      var res = jsonDecode(response["body"]);

      if (response["statusCode"] == 201) {
        EasyLoading.showSuccess(res["message"]);
      }
    });
  }
}
