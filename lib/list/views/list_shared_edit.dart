import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:listapp/list/model/list.dart';

class ListSharedEditPage extends StatefulWidget {
  final args;
  final String id;

  ListSharedEditPage({this.id, this.args});

  @override
  _ListSharedEditPageState createState() => _ListSharedEditPageState();
}

class _ListSharedEditPageState extends State<ListSharedEditPage> {
  final _newElementText = TextEditingController();
  var _isChecked = [];

  Lists list;
  StreamController<double> streamcontroller;

  var items = [];

  @override
  void initState() {
    super.initState();
    list = widget.args['list'];
    streamcontroller = widget.args['streamcontroller'];
    items = jsonDecode(list.items);
    _isChecked = List<bool>.filled(items.length, false);
  }

  @override
  void dispose() {
    _newElementText.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    EasyLoading.instance
      ..indicatorType = EasyLoadingIndicatorType.fadingCircle
      ..loadingStyle = EasyLoadingStyle.dark
      ..userInteractions = true
      ..dismissOnTap = true;

    Color getColor(Set<MaterialState> states) {
      const Set<MaterialState> interactiveStates = <MaterialState>{
        MaterialState.pressed,
        MaterialState.hovered,
        MaterialState.focused,
      };
      if (states.any(interactiveStates.contains)) {
        return Colors.blue;
      }
      return Colors.red;
    }

    return Scaffold(
        appBar: AppBar(
          title: Text(list.title),
          actions: [
            IconButton(
              icon: Icon(Icons.save),
              onPressed: () {
                EasyLoading.show(status: '');
                // Api().put('/' + list.id, items).then((response) {
                //   EasyLoading.showSuccess('Lista została zaktualizowana');
                //   var list = Lists.fromJson(response);
                //   setState(() {
                //     items = jsonDecode(list.items);
                //     streamcontroller.add(1);
                //   });
                // });
              },
            ),
          ],
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              setState(() {
                Navigator.of(context).pop();
              });
            },
          ),
        ),
        body: Column(mainAxisSize: MainAxisSize.min, children: [
          new Expanded(
              child: new ListView(physics: ScrollPhysics(), children: <Widget>[
            new ListView.builder(
              shrinkWrap: true,
              physics: ScrollPhysics(),
              itemCount: items.length,
              itemBuilder: (
                BuildContext context,
                int index,
              ) {
                var product = jsonDecode(items[index]);
                return new ListTile(
                  key: Key(product['text']),
                  leading: Checkbox(
                    checkColor: Colors.white,
                    fillColor: MaterialStateProperty.resolveWith(getColor),
                    value: product['checked'],
                    onChanged: (value) {
                      setState(() {
                        product['checked'] = value;
                        items[index] = jsonEncode(product);
                      });
                    },
                  ),
                  title: TextFormField(
                      key: Key(items[index]),
                      initialValue: product['text'],
                      onEditingComplete: () {
                        print(product);
                      },
                      onChanged: (value) {
                        // setState(() {
                        product['text'] = value;
                        items[index] = jsonEncode(product);
                        // });
                      },
                      style: TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        contentPadding: EdgeInsets.only(
                            left: 15, bottom: 11, top: 11, right: 15),
                      )),
                  trailing: IconButton(
                      icon: Icon(Icons.close),
                      iconSize: 14,
                      color: Colors.redAccent,
                      onPressed: () {
                        setState(() {
                          items.remove(jsonEncode(product));
                        });
                      }),
                );
              },
            ),
            Ink(
              color: Color.fromRGBO(211, 211, 211, 0.5),
              child: ListTile(
                title: TextFormField(
                    controller: _newElementText,
                    onFieldSubmitted: (value) {
                      setState(() {
                        if (_newElementText.text.length > 0) {
                          // _isChecked.add(false);
                          items.add(jsonEncode({
                            "text": _newElementText.text,
                            "checked": false
                          }));
                        }

                        _newElementText.clear();
                      });
                    },
                    decoration: InputDecoration(
                      hintText: 'Dodaj kolejny',
                      border: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      contentPadding: EdgeInsets.only(
                          left: 15, bottom: 11, top: 11, right: 15),
                    )),
                trailing: IconButton(
                    icon: Icon(Icons.add),
                    iconSize: 18,
                    color: Colors.black,
                    onPressed: () {
                      setState(() {
                        if (_newElementText.text.length > 0) {
                          // _isChecked.add(false);
                          items.add(jsonEncode({
                            "text": _newElementText.text,
                            "checked": false
                          }));
                        }

                        _newElementText.clear();
                      });
                    }),
              ),
            )
          ]))
        ]));
  }
}
