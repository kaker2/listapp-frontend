import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:listapp/common/FirebaseCloudMessage.dart';
import 'package:listapp/common/dialogs/infoDialog.dart';
import 'package:listapp/common/mixins/api_call.dart';
import 'package:listapp/common/services/sqlite_service.dart';
import 'package:listapp/common/syncDataWithServer.dart';
import 'package:listapp/list/model/list.dart';
import 'package:listapp/list/views/share_list.dart';
import 'package:listapp/common/routes/app_routes.dart';

class ListEditPage extends StatefulWidget {
  final args;
  final String id;

  ListEditPage({this.id, this.args});

  @override
  _ListEditPageState createState() => _ListEditPageState();
}

class _ListEditPageState extends State<ListEditPage> with ApiCall {
  final _newElementText = TextEditingController();
  static FocusNode _newElementTextFocus;
  static var _listTitle = TextEditingController();
  var _isChecked;
  var isEdited = false;
  Lists list;
  static var index;
  StreamController<double> streamcontroller;
  final sqliteService = Get.find<SqliteService>();

  var items = [];

  void asyncMethod(param) async {
    final response = await SyncDataWithServer().getSharedList(param['id']);
    setState(() {
      list = Lists.fromJson(response);
      items = (list.items.runtimeType == String)
          ? jsonDecode(list.items)
          : list.items;
    });
  }

  void getListFromNotify(String id) async {
    var localList = await sqliteService.findBy('lists', 'id = ?', [id]);
    localList = localList[0];
    setState(() {
      list = Lists.fromJson(localList);
      items = (list.items.runtimeType == String)
          ? jsonDecode(list.items)
          : list.items;
    });
  }

  @override
  void initState() {
    var param = Get.parameters;
    super.initState();
    this.initApi("lists");
    Future.delayed(Duration.zero, () async {
      if (param['id'] != null && param['id'] != ':id') {
        await asyncMethod(param);
      } else if (Get.arguments['id'] != null) {
        await getListFromNotify(Get.arguments['id']);
      } else {
        setState(() {
          list = Get.arguments['list'];
          index = Get.arguments['index'];
          streamcontroller = Get.arguments['streamcontroller'];
          items = (list.items.runtimeType == String)
              ? jsonDecode(list.items)
              : list.items;
        });
      }

      _listTitle.text = list.title;
    });
    _isChecked = List<bool>.filled(items.length, false);
    _newElementTextFocus = FocusNode();
  }

  @override
  void dispose() {
    _newElementText.dispose();
    super.dispose();
  }

  void save(showBadge) {
    if (showBadge) {
      EasyLoading.show(status: '');
    }

    this.putApi("/${list.id}", {
      'title': _listTitle.text,
      'items': json.encode(items)
    }).then((response) {
      isEdited = true;
      if (showBadge) {
        EasyLoading.showSuccess('Lista została zaktualizowana');
      }
      setState(() {
        items = items;
        streamcontroller.add(1);
      });
    });
  }

  _sendPushNotificationToUsers() {
    if (!isEdited) return false;
    FirebaseCloudMessage().sendNotificationByListId(list);
    return true;
  }

  @override
  Widget build(BuildContext context) {
    EasyLoading.instance
      ..indicatorType = EasyLoadingIndicatorType.fadingCircle
      ..loadingStyle = EasyLoadingStyle.dark
      ..userInteractions = true
      ..dismissOnTap = true;

    Color getColor(Set<MaterialState> states) {
      const Set<MaterialState> interactiveStates = <MaterialState>{
        MaterialState.pressed,
        MaterialState.hovered,
        MaterialState.focused,
      };
      if (states.any(interactiveStates.contains)) {
        return Colors.blue;
      }
      return Colors.red;
    }

    return WillPopScope(
        onWillPop: () {
          setState(() {
            Get.toNamed(AppRoutes.Lists);
          });
          return _sendPushNotificationToUsers();
        },
        child: Scaffold(
          appBar: AppBar(
            title: new TextField(
              controller: _listTitle,
              decoration: InputDecoration(
                fillColor: Colors.white,
                filled: true,
                hintText: 'Nazwa listy',
              ),
            ),
            actions: [
              PopupMenuButton(
                icon: Icon(Icons.more_vert),
                itemBuilder: (BuildContext context) => <PopupMenuEntry>[
                  PopupMenuItem(
                    child: ListTile(
                      leading: Icon(Icons.share),
                      title: Text('Udostępnij'),
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.push(
                          context,
                          MaterialPageRoute<void>(
                            builder: (BuildContext context) =>
                                new ShareList(data: list),
                            fullscreenDialog: true,
                          ),
                        );
                      },
                    ),
                  ),
                  PopupMenuItem(
                    child: ListTile(
                      leading: Icon(Icons.info),
                      title: Text('Szczegóły'),
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.push(
                          context,
                          MaterialPageRoute<void>(
                            builder: (BuildContext context) =>
                                new InfoDialog(data: list),
                            fullscreenDialog: true,
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ],
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                setState(() {
                  _sendPushNotificationToUsers();
                  Get.toNamed(AppRoutes.Lists);
                });
              },
            ),
          ),
          body: Column(mainAxisSize: MainAxisSize.min, children: [
            Ink(
              color: Color.fromRGBO(211, 211, 211, 0.5),
              child: ListTile(
                title: TextField(
                    controller: _newElementText,
                    focusNode: _newElementTextFocus,
                    onSubmitted: (value) {
                      setState(() {
                        if (_newElementText.text.length > 0) {
                          items.add(jsonEncode({
                            "text": _newElementText.text,
                            "checked": false
                          }));
                          _newElementText.clear();
                          _newElementTextFocus.requestFocus();
                          this.save(false);
                        }
                      });
                    },
                    decoration: InputDecoration(
                      hintText: 'Dodaj kolejny',
                      border: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      contentPadding: EdgeInsets.only(
                          left: 15, bottom: 11, top: 11, right: 15),
                    )),
                trailing: IconButton(
                    icon: Icon(Icons.add),
                    iconSize: 18,
                    color: Colors.black,
                    onPressed: () {
                      setState(() {
                        if (_newElementText.text.length > 0) {
                          items.add(jsonEncode({
                            "text": _newElementText.text,
                            "checked": false
                          }));
                          this.save(false);
                        }

                        _newElementText.clear();
                      });
                    }),
              ),
            ),
            new Expanded(
                child:
                    new ListView(physics: ScrollPhysics(), children: <Widget>[
              new ListView.builder(
                shrinkWrap: true,
                physics: ScrollPhysics(),
                itemCount: items.length,
                itemBuilder: (
                  BuildContext context,
                  int index,
                ) {
                  var product = jsonDecode(items[index]);

                  return new ListTile(
                    key: Key(product['text']),
                    leading: Checkbox(
                      checkColor: Colors.white,
                      fillColor: MaterialStateProperty.resolveWith(getColor),
                      value: product['checked'],
                      onChanged: (value) {
                        setState(() {
                          product['checked'] = value;
                          items[index] = jsonEncode(product);
                        });
                        this.save(false);
                      },
                    ),
                    title: TextFormField(
                        key: Key(items[index]),
                        initialValue: product['text'],
                        onEditingComplete: () {
                          print(product);
                        },
                        onChanged: (value) {
                          // setState(() {
                          product['text'] = value;
                          items[index] = jsonEncode(product);
                          // });
                        },
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          contentPadding: EdgeInsets.only(
                              left: 15, bottom: 11, top: 11, right: 15),
                        )),
                    trailing: IconButton(
                        icon: Icon(Icons.close),
                        iconSize: 14,
                        color: Colors.redAccent,
                        onPressed: () {
                          setState(() {
                            items.remove(jsonEncode(product));
                            this.save(false);
                          });
                        }),
                  );
                },
              ),
            ]))
          ]),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              this.save(true);
            },
            tooltip: "Zapisz",
            child: Icon(Icons.save),
            backgroundColor: Colors.green,
          ),
        ));
  }
}
