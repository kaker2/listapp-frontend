import 'dart:async';

import 'package:get/get.dart';
import 'package:listapp/auth/services/auth_service.dart';
import 'package:listapp/common/StoreBase.dart';
import 'package:listapp/common/LocalNotificationBase.dart';
import 'package:listapp/common/dialogs/infoDialog.dart';
import 'package:listapp/common/mixins/api_call.dart';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:listapp/common/services/sqlite_service.dart';
import 'package:listapp/list/views/share_list.dart';
import 'package:listapp/utils/drawer.dart';
import 'package:listapp/list/model/list.dart';
import 'package:listapp/common/routes/app_routes.dart';

class ListPage extends StatefulWidget {
  @override
  _ListPageState createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> with ApiCall {
  List<Lists> listModel = [];
  Stream stream;
  StreamController<double> controller;
  var sqliteService = Get.find<SqliteService>();
  var isConnected = false;
  final authService = Get.find<AuthService>();

  _ListPageState() {
    this.initApi("lists");
  }

  @override
  void initState() {
    super.initState();
    controller = StreamController<double>();
    stream = controller.stream;
    stream.listen((value) {
      if (value == 1) {
        listModel.clear();
        getData();
      }
    });
    getData();
  }

  // Make sure to cancel subscription after you are done
  @override
  dispose() {
    super.dispose();
  }

  void setData(bool api, items) {
    setState(() {
      for (var json in items) {
        if (api) {
          sqliteService.insert(json);
        }
        listModel.add(Lists.fromJson(json));
      }
    });
  }

  void getData() async {
    var getLists = await sqliteService.get('lists', 'createdBy = ?', ['false']);
    var api = false;

    if (getLists.length <= 0) {
      if (StoreBase().getIsConnected()) {
        getLists = this.getApi("");
        api = true;
        getLists.then((response) {
          setData(api, response['lists']);
        });
      }
      return;
    }
    setData(api, getLists);
  }

  var i = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Listy'),
        actions: [
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () {
              Get.toNamed(AppRoutes.ListsCreate);
            },
          ),
        ],
      ),
      drawer: CustomDrawer(),
      body: new ListView.builder(
        shrinkWrap: true,
        physics: ScrollPhysics(),
        itemCount: listModel.length,
        itemBuilder: (
          BuildContext context,
          int index,
        ) {
          var list = listModel[index];
          return Card(
              child: ListTile(
                  title: Text(list.title),
                  leading: Text(
                    list.checked.toString() + "/" + list.totalItems.toString(),
                    style: TextStyle(
                      color: (list.checked == list.totalItems)
                          ? Colors.green
                          : Colors.black26,
                    ),
                  ),
                  trailing: PopupMenuButton(
                      icon: Icon(Icons.more_vert),
                      itemBuilder: (BuildContext context) => <PopupMenuEntry>[
                            PopupMenuItem(
                              child: ListTile(
                                leading: Icon(Icons.share),
                                title: Text('Udostępnij'),
                                onTap: () {
                                  Navigator.pop(context);
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute<void>(
                                      builder: (BuildContext context) =>
                                          new ShareList(data: list),
                                      fullscreenDialog: true,
                                    ),
                                  );
                                },
                              ),
                            ),
                            PopupMenuItem(
                              child: ListTile(
                                leading: Icon(Icons.edit),
                                title: Text('Edytuj'),
                                onTap: () {
                                  Navigator.pop(context);
                                  Get.toNamed(AppRoutes.ListsEdit, arguments: {
                                    'list': list,
                                    'index': index,
                                    'streamcontroller': controller
                                  });
                                },
                              ),
                            ),
                            PopupMenuItem(
                              child: ListTile(
                                leading: Icon(Icons.info),
                                title: Text('Szczegóły'),
                                onTap: () {
                                  Navigator.pop(context);
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute<void>(
                                      builder: (BuildContext context) =>
                                          new InfoDialog(data: list),
                                      fullscreenDialog: true,
                                    ),
                                  );
                                },
                              ),
                            ),
                            PopupMenuItem(
                              child: ListTile(
                                  leading: Icon(Icons.delete),
                                  title: Text('Usuń'),
                                  onTap: () {
                                    Navigator.pop(context);
                                    return _showDialog(list, index);
                                  }),
                            )
                          ]),
                  onTap: () {
                    Get.toNamed(AppRoutes.ListsEdit, arguments: {
                      'list': list,
                      'index': index,
                      'streamcontroller': controller
                    });
                  }));
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Get.toNamed(AppRoutes.ListsCreate);
        },
        tooltip: "Stwórz liste",
        child: Icon(Icons.add),
      ),
    );
  }

  void _showDialog(list, index) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Czy napewno chcesz usunąć tą listę"),
          content: new Text(
              "(lista zostanie usunięta u ciebie i u wszystkich użytkowników  gdzie była udostępniona)"),
          actions: <Widget>[
            new FlatButton(
              color: Colors.red,
              child: new Text("Usuń"),
              onPressed: () {
                this.deleteApi(list.id).then((response) {
                  setState(() {
                    Navigator.pop(context);
                    EasyLoading.show(status: 'Usuwanie');
                    listModel.removeAt(index);
                    EasyLoading.showSuccess('Lista została usunięta');
                  });
                });
              },
            ),
            new FlatButton(
              child: new Text("Anuluj"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}

const margin = EdgeInsets.only(top: 20, left: 20);
