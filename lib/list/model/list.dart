import 'dart:convert' as conv;

import 'dart:math';
import 'package:uuid/uuid.dart';
import 'package:listapp/utils/constants.dart' as CONST;

class Lists {
  String id;
  String title;
  var items;
  int createdBy;
  var is_on_server;
  String created_at;
  String updated_at;
  String deleted_at;
  var checked;
  var totalItems;
  String share_url;
  DateTime dateToday = DateTime(
      DateTime.now().year,
      DateTime.now().month,
      DateTime.now().day,
      DateTime.now().hour,
      DateTime.now().minute,
      DateTime.now().second);
  var uuid = Uuid();

  Lists(
      {this.id,
      this.title,
      this.items,
      this.is_on_server,
      this.createdBy,
      this.created_at,
      this.updated_at,
      this.deleted_at,
      this.checked,
      this.totalItems,
      this.share_url});

  Lists.fromJson(json) {
    id = (json['id'] != '') ? json['id'].toString() : uuid.v4();

    title = (json['title'] != null) ? json['title'] : '';
    items = (json['items'] != null) ? json['items'] : '';
    is_on_server = (json['is_on_server'] == null) ? 0 : json['is_on_server'];
    createdBy = (json['created_by'] != null) ? json['created_by'] : 0;
    created_at = (json['created_at'] != null)
        ? json['created_at']
        : dateToday.toString();
    updated_at = (json['updated_at'] != null) ? json['updated_at'] : '';
    deleted_at = (json['deleted_at'] != null) ? json['deleted_at'] : null;

    this.getCheckedItems();

    checked = this.checked;
    totalItems = this.totalItems;
    share_url = CONST.LIST_SHARED_URL + '/' + this.id;
  }

  Map<String, dynamic> toMap() {
    return {
      'id': this.id,
      'title': this.title,
      'items': (this.items.runtimeType != String)
          ? conv.jsonEncode(this.items)
          : this.items,
      'is_on_server': this.is_on_server,
      'created_at': this.created_at,
      'updated_at': this.updated_at,
      'deleted_at': this.deleted_at,
      'createdBy': this.createdBy
    };
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['items'] = this.items;
    data['is_on_server'] = this.is_on_server;
    data['created_by'] = this.createdBy;
    data['created_at'] = this.created_at;
    data['updated_at'] = this.updated_at;
    data['deleted_at'] = this.deleted_at;
    data['checked'] = this.checked;
    data['totalItems'] = this.totalItems;
    return data;
  }

  void getCheckedItems() {
    var items = (this.items.runtimeType != String)
        ? this.items
        : conv.jsonDecode(this.items);

    var checked = 0;
    items = (items.runtimeType == String) ? conv.jsonDecode(items) : items;
    if (items != null) {
      for (var item in items) {
        var i = conv.jsonDecode(item);
        if (i['checked']) {
          checked++;
        }
      }
    }

    this.checked = checked;
    this.totalItems = (items != null) ? items.length : 0;
  }
}
