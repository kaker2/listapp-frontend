import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:http/http.dart';
import 'package:listapp/common/services/sqlite_service.dart';

class AuthService {
  String _token;
  final _storage = new FlutterSecureStorage();
  var sqliteService = Get.find<SqliteService>();

  AuthService() {}

  void initState() {
    getToken();
  }

  getToken() async {
    _token = await _storage.read(key: 'jwt');
    return _token;
  }

  hasToken() async {
    var token = await getToken();
    return token;
  }

  getJwt() {
    return _token;
  }

  getJsonFromJWT() {
    final parts = getJwt().split('.');
    if (parts.length != 3) {
      throw Exception('invalid token');
    }

    final payload = _decodeBase64(parts[1]);
    final payloadMap = json.decode(payload);
    if (payloadMap is! Map<String, dynamic>) {
      throw Exception('invalid payload');
    }

    return payloadMap;
  }

  String _decodeBase64(String str) {
    String output = str.replaceAll('-', '+').replaceAll('_', '/');

    switch (output.length % 4) {
      case 0:
        break;
      case 2:
        output += '==';
        break;
      case 3:
        output += '=';
        break;
      default:
        throw Exception('Illegal base64url string!"');
    }

    return utf8.decode(base64Url.decode(output));
  }

  deleteData(String key) async {
    var deleteData = await _storage.delete(key: key);
    sqliteService.deleteDatabases();
    return deleteData;
  }
}
