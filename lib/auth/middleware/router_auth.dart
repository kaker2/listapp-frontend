import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:listapp/auth/services/auth_service.dart';
import 'package:listapp/common/routes/app_routes.dart';

class RouteAuthMiddleware extends GetMiddleware {
  @override
  int priority = 0;

  final authService = Get.find<AuthService>();

  RouteAuthMiddleware({this.priority}) {}

  @override
  RouteSettings redirect(String route) {
    var jwt = (Get.parameters['token'] != null && Get.parameters['token'] != '')
        ? Get.parameters['token']
        : authService.getJwt();
    return jwt != null && jwt != ''
        ? null
        : RouteSettings(name: AppRoutes.Unauthorized);
  }
}
