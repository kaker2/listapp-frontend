import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:listapp/auth/module.dart';
import 'package:listapp/common/FirebaseCloudMessage.dart';
import 'package:listapp/utils/constants.dart' as CONST;
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:listapp/utils/helpers.dart';
import 'package:listapp/utils/extensions.dart';
import 'dart:developer' as developer;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:listapp/common/routes/app_routes.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> with Module, Helpers {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final _storage = const FlutterSecureStorage();
  final fcm = new FirebaseCloudMessage();
  String errors = "";

  var fields_object = {"email": "", "password": "", "fcm_token": ""};

  @override
  void initState() {
    super.initState();
  }

  void validateAndSave(context) {
    final FormState form = _formKey.currentState;
    if (form.validate()) {
      login(context);
    } else {
      print('Form is invalid');
    }
  }

  void login(context) async {
    EasyLoading.show(
      status: this.get('lang', context).please_wait,
    );

    try {
      await fcm.setToken();
      fields_object['fcm_token'] = fcm.getToken();

      var url = Uri.parse(CONST.API_V1_URL + '/auth/login');
      var post = await http.post(url, body: fields_object);
      var body = jsonDecode(post.body);

      if (![200, 201, 202, 203, 204, 205].contains(post.statusCode)) {
        setState(() {
          if (body['lang'] == 'auth_wrong_email_or_password') {
            errors = AppLocalizations.of(context).auth_wrong_email_or_password;
          }
        });
        EasyLoading.dismiss();

        return developer.log(
            'Response status: ${post.statusCode} | Response body: ${post.body}',
            name: 'login');
      }
      if (body['access_token']?.isNotEmpty == true) {
        await this._getAndSaveToken(body['access_token']);
        EasyLoading.dismiss();
        Get.toNamed(AppRoutes.Lists,
            parameters: <String, String>{"token": body['access_token']});
      }
    } catch (e) {
      EasyLoading.showInfo('Brak połączenia z serwerem');
      return;
    }
  }

  void _getAndSaveToken(token) {
    if (token != "") {
      _storage.write(
        key: 'jwt',
        value: token,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: Container(
      padding: EdgeInsets.only(left: 15.0, right: 15.0),
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
                padding: EdgeInsets.only(left: 15.0, right: 15.0),
                child: Text(
                  errors,
                  style:
                      TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
                )),
            // input field for email
            TextFormField(
                decoration: InputDecoration(
                    labelText: Module().get('lang', context).email),
                validator: (value) {
                  value.isValidEmail();
                  if (value.isEmpty) {
                    return Module().get('lang', context).empty_field;
                  } else {
                    if (!value.isValidEmail()) {
                      return Module()
                          .get('lang', context)
                          .email_error_valid_email;
                    }
                    fields_object["email"] = value;
                    return null;
                  }
                }
                // onSaved: (value) => _email = value,
                ),
            // input field for password
            TextFormField(
                decoration: InputDecoration(
                    labelText: Module().get('lang', context).password),
                obscureText: true,
                validator: (String value) {
                  if (value.isEmpty) {
                    return Module().get('lang', context).empty_field;
                  } else {
                    if (value.length < CONST.MIN_LENGTH_PASSWORD) {
                      return Module()
                          .get('lang', context)
                          .password_error_length;
                    }
                    fields_object["password"] = value;
                    return null;
                  }
                }),

            ElevatedButton(
              child: Text(
                Module().get('lang', context).signin,
                style: TextStyle(fontSize: 20.0),
              ),
              style: ElevatedButton.styleFrom(primary: Colors.green),
              onPressed: () {
                validateAndSave(context);
              },
            ),
            Text(Module().get('lang', context).or),
            ElevatedButton(
              child: Text(
                Module().get('lang', context).signup,
                style: TextStyle(
                  fontSize: 20.0,
                ),
              ),
              onPressed: () {
                Get.toNamed(AppRoutes.AuthRegister);
              },
            ),
          ],
        ),
      ),
    )));
  }
}
