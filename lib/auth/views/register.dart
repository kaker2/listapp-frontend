import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:http/http.dart';
import 'package:listapp/utils/constants.dart' as CONST;
import 'package:listapp/utils/api.dart';
import 'package:listapp/utils/extensions.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:listapp/auth/module.dart';
import 'package:listapp/utils/helpers.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> with Module, Helpers {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _confirmPasswordController = TextEditingController();
  FocusNode _focusEmail = new FocusNode();
  final GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();

  String errors = "";

  var json = {};

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _focusEmail.dispose();
  }

  void validateAndSave() {
    final FormState form = _formKey.currentState;
    if (form.validate()) {
      register();
    }
  }

  void register() {
    EasyLoading.show(
      status: this.get('lang', context).please_wait,
    );
    Api().post(this.prefix + "/register", json).then((response) {
      EasyLoading.dismiss();
      var res = jsonDecode(response['body']);

      if (res['status'] != 200) {
        if (res['lang'] == 'auth_email_found') {
          setState(() {
            errors = AppLocalizations.of(context).auth_email_found;
          });
          return;
        }
      }
      EasyLoading.showSuccess(
          AppLocalizations.of(context).auth_registration_success);
      // EasyLoading.dismiss();
      Get.toNamed("/auth/login");
    });
  }

  @override
  Widget build(BuildContext context) {
    var password = '';

    return Container(
      padding: EdgeInsets.only(left: 15.0, right: 15.0),
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
                padding: EdgeInsets.only(left: 15.0, right: 15.0),
                child: Text(
                  errors,
                  style:
                      TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
                )),
            // input field for email
            TextFormField(
              controller: _emailController,
              decoration: InputDecoration(
                  labelText: AppLocalizations.of(context).email),
              validator: (value) {
                if (value.isEmpty) {
                  return AppLocalizations.of(context).empty_field;
                } else {
                  if (!value.isValidEmail()) {
                    return AppLocalizations.of(context).email_error_valid_email;
                  }
                  json['email'] = value;
                  return null;
                }
              },
              focusNode: _focusEmail,
              // onSaved: (value) => _email = value,
            ),
            // input field for password
            TextFormField(
                controller: _passwordController,
                decoration: InputDecoration(
                    labelText: AppLocalizations.of(context).password),
                obscureText: true,
                validator: (String value) {
                  password = value;

                  if (value.isEmpty) {
                    return AppLocalizations.of(context).empty_field;
                  } else {
                    if (value.length < CONST.MIN_LENGTH_PASSWORD) {
                      return AppLocalizations.of(context).password_error_length;
                    }
                    json['password'] = value;
                    return null;
                  }
                }),
            TextFormField(
                controller: _confirmPasswordController,
                decoration: InputDecoration(
                    labelText: AppLocalizations.of(context).confirm_password),
                obscureText: true,
                validator: (String value) {
                  if (value.isEmpty) {
                    return AppLocalizations.of(context).empty_field;
                  } else if (value != password) {
                    return AppLocalizations.of(context)
                        .confirm_password_error_message;
                  }
                  json['confirm_password'] = value;
                  return null;
                }),
            ElevatedButton(
              child: Text(
                AppLocalizations.of(context).signup,
                style: TextStyle(fontSize: 20.0),
              ),
              onPressed: validateAndSave,
            ),
            Text(AppLocalizations.of(context).or),
            ElevatedButton(
              child: Text(
                AppLocalizations.of(context).signin,
                style: TextStyle(
                  fontSize: 20.0,
                ),
              ),
              onPressed: () {
                Get.toNamed("/auth/login");
              },
            ),
          ],
        ),
      ),
    );
  }
}
