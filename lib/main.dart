import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:listapp/auth/services/auth_service.dart';
import 'package:listapp/common/FirebaseCloudMessage.dart';
import 'package:listapp/common/LocalNotificationBase.dart';
import 'package:listapp/common/checkNetworkConnection.dart';
import 'package:listapp/common/routes/app_pages.dart';
import 'package:listapp/common/routes/app_routes.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:listapp/common/services/sqlite_service.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:uni_links/uni_links.dart';
import 'package:firebase_core/firebase_core.dart';
// import 'firebase_options.dart';

//ADD THIS FUNCTION TO HANDLE DEEP LINKS
final _storage = FlutterSecureStorage();
final authService = Get.find<AuthService>();
final fcm = new FirebaseCloudMessage();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  LocalNotificationBase.initNotification();
  Get.put(SqliteService());
  Get.put(AuthService());
  CheckNetworkConnection();
  await Firebase.initializeApp(
      // options: DefaultFirebaseOptions.currentPlatform,
      );

  fcm.getMessage();

  runApp(
    GetMaterialApp(
        localizationsDelegates: [
          AppLocalizations.delegate, // Add this line
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: [
          Locale('en', ''),
          Locale('pl', '')
        ],
        locale: Locale.fromSubtags(languageCode: 'pl'),
        initialRoute: await setInitialRoute(),
        unknownRoute: AppPages.unknownRoute,
        getPages: AppPages.routes,
        builder: EasyLoading.init()),
  );

  initUniLinks();

  configLoading();
}

Future<Null> initUniLinks() async {
  try {
    Uri initialLink = await getInitialUri();
    print(initialLink);
  } on PlatformException {
    print('platfrom exception unilink');
  }
}

setInitialRoute() async {
  var token = await authService.hasToken();
  if (token != null) {
    return AppRoutes.Lists;
  }
  return AppRoutes.AuthLogin;
}

jwtOrEmpty() {
  var jwt = _storage.read(key: "jwt");
  if (jwt == null) return false;
  return jwt;
}

void configLoading() {
  EasyLoading.instance
    ..indicatorType = EasyLoadingIndicatorType.fadingCircle
    ..loadingStyle = EasyLoadingStyle.dark
    ..userInteractions = false
    ..maskColor = Colors.black45
    ..maskType = EasyLoadingMaskType.custom
    ..dismissOnTap = false;
}
