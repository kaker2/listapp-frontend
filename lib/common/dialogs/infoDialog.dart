import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:listapp/list/model/list.dart';
import 'package:listapp/list/views/list.dart';

class InfoDialog extends StatelessWidget {
  Lists data;
  InfoDialog({this.data});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Szczegóły"),
        ),
        body: Table(
          columnWidths: const <int, TableColumnWidth>{
            0: FlexColumnWidth(),
            1: FlexColumnWidth(),
          },
          defaultVerticalAlignment: TableCellVerticalAlignment.middle,
          children: <TableRow>[
            TableRow(
              children: <Widget>[
                Container(margin: margin, child: Text('Data stworzenia')),
                Container(margin: margin, child: Text(this.data.created_at)),
              ],
            ),
            TableRow(
              children: <Widget>[
                Container(margin: margin, child: Text('Dodane przez')),
                Container(margin: margin, child: Text('Kacper')),
              ],
            ),
          ],
        ));
  }
}
