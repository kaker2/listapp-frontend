import 'dart:convert';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:listapp/common/LocalNotificationBase.dart';
import 'package:listapp/common/mixins/api_call.dart';
import 'package:listapp/list/model/list.dart';

class FirebaseCloudMessage with ApiCall {
  String token;
  FirebaseMessaging fcm = FirebaseMessaging.instance;

  FirebaseCloudMessage() {
    init();
  }

  init() async {
    this.initApi("custom-fcm");

    await fcm.setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
    );
  }

  getToken() {
    return this.token;
  }

  setToken() async {
    this.token = await fcm.getToken();
  }

  sendNotificationByListId(list) async {
    await this.getApi("/${list.id}").then((response) {
      print({'response': response});
    });
  }

  getMessage() {
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      LocalNotificationBase.setNotificationMessage(
          1, message.data['title'], message.data['body'], 'true');

      if (message.notification != null) {
        print('Message also contained a notification: ${message.notification}');
      }
    });
  }
}
