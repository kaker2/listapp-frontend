import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:get/get.dart';
import 'package:listapp/common/routes/app_routes.dart';

class UnauthorizedPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context).unauthorized_code),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 15.0, right: 15.0),
            child: ListTile(
              title: Text(AppLocalizations.of(context).unauthorized_message),
              subtitle: Text(AppLocalizations.of(context).unauthorized_code),
              onTap: () => Get.toNamed(AppRoutes.AuthLogin),
            ),
          ),
          // input field for email
        ],
      ),
    );
  }
}
