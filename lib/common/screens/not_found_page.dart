import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class NotfoundPage extends StatelessWidget {
  const NotfoundPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context).not_found_code),
      ),
      body: ListTile(
        title: Text(AppLocalizations.of(context).not_found_message),
        subtitle: Text(AppLocalizations.of(context).not_found_sub_message),
        onTap: () => '',
      ),
    );
  }
}
