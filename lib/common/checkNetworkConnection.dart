import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:listapp/common/StoreBase.dart';
import 'package:listapp/common/syncDataWithServer.dart';

class CheckNetworkConnection {
  StreamController<bool> isConnected = StreamController<bool>();

  CheckNetworkConnection() {
    checkConnection();
  }

  checkConnection() async {
    await Connectivity().onConnectivityChanged.listen((result) {
      if (result == ConnectivityResult.none) {
        StoreBase().setIsConnected(false);
      } else {
        StoreBase().setIsConnected(true);
        SyncDataWithServer();
      }
    });
    return isConnected;
  }
}
