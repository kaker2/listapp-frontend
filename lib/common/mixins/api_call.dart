import 'dart:math';

import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:listapp/auth/services/auth_service.dart';
import 'package:listapp/common/StoreBase.dart';
import 'package:listapp/common/services/sqlite_service.dart';
import 'package:listapp/utils/constants.dart' as CONST;
import 'dart:convert';
import 'package:uuid/uuid.dart';

mixin ApiCall {
  String api_url;
  String class_url;
  final authService = Get.find<AuthService>();
  var sqliteService = Get.find<SqliteService>();
  DateTime dateToday = DateTime(
      DateTime.now().year,
      DateTime.now().month,
      DateTime.now().day,
      DateTime.now().hour,
      DateTime.now().minute,
      DateTime.now().second);
  var uuid = Uuid();

  Map<String, String> requestHeaders = {};

  void initApi(String class_url) async {
    this.class_url = (class_url != null) ? class_url : '';
    setApiUrl();
  }

  setApiUrl() {
    this.api_url = "${CONST.API_V1_URL}/${this.class_url}";
  }

  setHeaders() async {
    return {
      'Authorization': "Bearer ${await this.authService.getToken()}",
    };
  }

  getHeaders() async {
    return await this.setHeaders();
  }

  getApi(String url) async {
    final response = await http.get(Uri.parse(this.api_url + url),
        headers: await this.getHeaders());

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      EasyLoading.dismiss();

      // return Get.toNamed(AppRoutes.Unauthorized);
    }
  }

  postWithoutSqlite(url, data) async {
    var response = null;
    try {
      if (StoreBase().getIsConnected()) {
        var request = await http.post(Uri.parse(this.api_url + url),
            body: data, headers: await this.getHeaders());
        if (request.statusCode != 200 && request.statusCode != 201) {
          throw Exception(
              'Response status: ${request.statusCode} | Response body: ${request.body}');
        }
        response = {"statusCode": request.statusCode, "body": request.body};
        return response;
      } else {
        return {"statusCode": 400, "body": "Brak połączenia internetowego"};
      }
    } catch (e) {
      print({"catch": e.toString()});
    }
  }

  postApi(String url, data) async {
    var response = null;
    try {
      var sqlite = await saveDataInSqliteService(data);
      if (StoreBase().getIsConnected()) {
        var request = await http.post(Uri.parse(this.api_url + url),
            body: data, headers: await this.getHeaders());

        if (request.statusCode != 200 && request.statusCode != 201) {
          throw Exception(
              'Response status: ${request.statusCode} | Response body: ${request.body}');
        }
      }

      response = {"statusCode": sqlite.statusCode, "body": sqlite.body};
      return response;
    } catch (e) {
      print(e);
    }
  }

  saveDataInSqliteService(data) async {
    data['id'] = uuid.v4();
    data['created_at'] = dateToday.toString();
    data['updated_at'] = dateToday.toString();
    data['is_on_server'] = (StoreBase().getIsConnected()) ? "true" : "false";

    var sql = await sqliteService.insert(data);

    return {"statusCode": 201, "body": sql, 'id': data['id']};
  }

  putApi(String url, data) async {
    try {
      var id = url.split('/')[1];
      var sql = await sqliteService.update(this.class_url, id, data);

      if (StoreBase().getIsConnected()) {
        var request = await http.put(Uri.parse(this.api_url + url),
            body: data, headers: await this.getHeaders());

        if (request.statusCode == 200) {
          return json.decode(request.body);
        } else {
          throw Exception('Failed to load lists');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  deleteApi(String id) async {
    var res;
    try {
      var sql = await sqliteService.delete(this.class_url, id);
      res = {
        "statusCode": 201,
        "body": sql,
      };
      if (StoreBase().getIsConnected()) {
        var request = await http.delete(Uri.parse(this.api_url + '/' + id),
            headers: await this.getHeaders());
        res = {
          "statusCode": request.statusCode,
          "body": request.body,
        };
      }
    } catch (e) {
      print(e);
    }
    return res;
  }
}
