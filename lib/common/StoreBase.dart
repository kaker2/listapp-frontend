import 'package:mobx/mobx.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class StoreBase {
  static final StoreBase _singleton = StoreBase._internal();
  bool is_connected = false;

  static final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  // initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
  static final AndroidInitializationSettings initializationSettingsAndroid =
      AndroidInitializationSettings('@mipmap/ic_launcher');
  static final IOSInitializationSettings initializationSettingsIOS =
      IOSInitializationSettings();
  static final AndroidNotificationDetails androidPlatformChannelSpecifics =
      AndroidNotificationDetails('your channel id', 'your channel name',
          channelDescription: 'your channel description',
          importance: Importance.max,
          priority: Priority.high,
          ticker: 'ticker');
  static final NotificationDetails platformChannelSpecifics =
      NotificationDetails(android: androidPlatformChannelSpecifics);

  factory StoreBase() {
    return _singleton;
  }

  void setIsConnected(status) {
    is_connected = status;
  }

  void initNotification() async {
    final InitializationSettings initializationSettings =
        InitializationSettings(
            android: initializationSettingsAndroid,
            iOS: initializationSettingsIOS);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings);
    NotificationDetails(android: androidPlatformChannelSpecifics);
  }

  static setNotificationMessage(i, String title, String body) async {
    await flutterLocalNotificationsPlugin
        .show(i, title, body, platformChannelSpecifics, payload: 'item x');
  }

  getIsConnected() {
    return is_connected;
  }

  StoreBase._internal();
}
