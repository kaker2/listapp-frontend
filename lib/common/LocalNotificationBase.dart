import 'package:flutter/rendering.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:listapp/common/routes/app_routes.dart';

class LocalNotificationBase {
  static final LocalNotificationBase _singleton =
      LocalNotificationBase._internal();

  static final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  static final AndroidInitializationSettings initializationSettingsAndroid =
      AndroidInitializationSettings('@mipmap/ic_launcher');
  static final IOSInitializationSettings initializationSettingsIOS =
      IOSInitializationSettings();

  static final AndroidNotificationDetails androidPlatformChannelSpecifics =
      AndroidNotificationDetails('your channel id', 'your channel name',
          channelDescription: 'your channel description',
          importance: Importance.max,
          priority: Priority.max,
          ticker: 'ticker');

  static final NotificationDetails platformChannelSpecifics =
      NotificationDetails(android: androidPlatformChannelSpecifics);

  factory LocalNotificationBase() {
    return _singleton;
  }

  static initNotification() async {
    final InitializationSettings initializationSettings =
        InitializationSettings(
            android: initializationSettingsAndroid,
            iOS: initializationSettingsIOS);
    await flutterLocalNotificationsPlugin.initialize(
      initializationSettings,
      onSelectNotification: selectNotification,
    );
    NotificationDetails(
      android: androidPlatformChannelSpecifics,
    );
  }

  static void selectNotification(String payload) async {
    if (payload != null) {
      debugPrint('notification payload: $payload');
    }
    await Get.toNamed(AppRoutes.ListsEdit, arguments: {"id": payload});
  }

  static setNotificationMessage(
      i, String title, String body, String payload) async {
    await flutterLocalNotificationsPlugin
        .show(i, title, body, platformChannelSpecifics, payload: payload);
  }

  LocalNotificationBase._internal();
}
