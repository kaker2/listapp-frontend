import 'dart:convert';

import 'package:get/get.dart';
import 'package:listapp/common/StoreBase.dart';
import 'package:listapp/common/mixins/api_call.dart';
import 'package:listapp/common/services/sqlite_service.dart';
import 'package:listapp/utils/constants.dart' as CONST;
import 'package:http/http.dart' as http;

class SyncDataWithServer with ApiCall {
  var sqliteService = Get.find<SqliteService>();

  SyncDataWithServer() {
    syncLists();
  }

  syncLists() async {
    if (!StoreBase().getIsConnected()) {
      return;
    }
    var table = 'lists';
    var lists = await sqliteService
        .findBy(table, 'is_on_server = ? AND id != ?', ['false', 'null']);
    if (lists.length <= 0) {
      return;
    }

    await http.post(Uri.parse(CONST.API_V1_URL + "/sync/lists"), body: {
      'lists': jsonEncode(lists),
    }, headers: {
      'Authorization': "Bearer ${await this.authService.getToken()}",
    }).then((value) => {
          lists.where((item) => item['id'] != 'null').forEach((list) {
            sqliteService
                .update(table, list['id'], {"is_on_server": true.toString()});
          })
        });
  }

  getSharedList(id) async {
    if (!StoreBase().getIsConnected()) {
      return;
    }
    var list = null;
    var saveList = (data) {
      sqliteService.insert(data);
    };
    await http
        .post(Uri.parse(CONST.API_V1_URL + "/sync/get-shared-list"), body: {
      'id': id
    }, headers: {
      'Authorization': "Bearer ${await this.authService.getToken()}",
    }).then((res) {
      if (res.statusCode == 201) {
        var body = json.decode(res.body);
        saveList(body['list']);
        list = body['list'];
      }
    }).catchError((error) => print(error));

    return list;
  }
}
