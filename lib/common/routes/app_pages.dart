import 'package:get/get.dart';
import 'package:listapp/auth/middleware/router_auth.dart';
import 'package:listapp/auth/views/login.dart';
import 'package:listapp/auth/views/register.dart';

import 'package:listapp/common/routes/app_routes.dart';
import 'package:listapp/common/screens/unauthorized.dart';
import 'package:listapp/list/views/create.dart';
import 'package:listapp/list/views/edit.dart';
import 'package:listapp/list/views/list.dart';
import 'package:listapp/common/screens/not_found_page.dart';

class AppPages {
  static const INITIAL = AppRoutes.Home;

  static final routes = [
    GetPage(
      name: AppRoutes.Unauthorized,
      page: () => UnauthorizedPage(),
    ),
    GetPage(
      name: AppRoutes.NotFound,
      page: () => NotfoundPage(),
    ),
    GetPage(
      name: AppRoutes.AuthLogin,
      page: () => LoginPage(),
    ),
    GetPage(
      name: AppRoutes.AuthRegister,
      page: () => RegisterPage(),
    ),
    GetPage(
      name: AppRoutes.Lists,
      page: () => ListPage(),
      middlewares: [
        RouteAuthMiddleware(priority: 1),
      ],
    ),
    GetPage(
      name: AppRoutes.ListsCreate,
      page: () => ListCreatePage(),
      middlewares: [
        RouteAuthMiddleware(priority: 1),
      ],
    ),
    GetPage(
      name: AppRoutes.ListsEdit,
      page: () => ListEditPage(),
      middlewares: [
        RouteAuthMiddleware(priority: 1),
      ],
    ),
  ];

  static final unknownRoute = GetPage(
    name: AppRoutes.NotFound,
    page: () => NotfoundPage(),
  );
}
