abstract class AppRoutes {
  static const Home = '/home';
  static const NotFound = '/not-found';
  static const AuthLogin = '/auth/login';
  static const AuthRegister = '/auth/register';
  static const AuthLogout = '/auth/logout';
  static const Unauthorized = '/unauthorized';
  static const Lists = '/lists';
  static const ListsCreate = '/lists/create';
  static const ListsEdit = '/lists/edit/:id';
  static const TestAuthRoute = '/test-auth-route';
}

// var routes = <String, WidgetBuilder>{
//   '/test-route': (context) => TestRoutePage(),

//   '/login': (context) => LoginPage(),
//   '/register': (context) => RegisterPage(),

//   '/lists': (context) => ListPage(),
//   '/lists/add': (context) => ListCreatePage(),
//   '/lists/edit': (context) =>
//       ListEditPage(args: ModalRoute.of(context).settings.arguments),
//   '/lists/shared/edit/{id}': (context) => ListSharedEditPage(),

//   '/shared': (context) => SharedPage(),
//   '/friends': (context) => FriendsPage(),
//   '/settings': (context) => SettingsPage(),
//   '/settings/language': (context) => SettingsLanguagePage(),
//   // '/settings/language': (context) => SettingsLanguagePage()
// };
