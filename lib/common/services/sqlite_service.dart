// import 'dart:async';
// import 'dart:ffi';

import 'package:listapp/list/model/list.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class SqliteService {
  var db;
  SqliteService() {
    initDatabase();
  }

  void initState() async {
    await initDatabase();
  }

  initDatabase() async {
    this.db = openDatabase(
      join(await getDatabasesPath(), 'listapp_database.db'),
      onCreate: (db, version) {
        // Run the CREATE TABLE statement on the database.
        return db.execute(
            'CREATE TABLE lists (id TEXT PRIMARY KEY,title TEXT NULL,items TEXT NULL,is_on_server BOOLEAN DEFAULT FALSE,createdBy TEXT NULL,created_at timestamp NULL,updated_at timestamp NULL,deleted_at timestamp NULL);');
      },
      // Set the version. This executes the onCreate function and provides a
      // path to perform database upgrades and downgrades.
      version: 1,
    );
    return this.db;
  }

  get(String table, where, args) async {
    var db = await initDatabase();

    return await db.query(table, orderBy: "created_at DESC");
    // where: where, whereArgs: args, orderBy: "created_at DESC");
  }

  findBy(String table, String where, args) async {
    var db = await initDatabase();
    return await db.query(table, where: where, whereArgs: args);
  }

  deleteDatabases() async {
    // Get a location using getDatabasesPath
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'listapp_database.db');

    // Delete the database
    await deleteDatabase(path);
  }

  insert(list) async {
    var db = await initDatabase();

    await db.insert(
      'lists',
      Lists.fromJson(list).toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  update(table, id, data) async {
    var db = await initDatabase();
    print({id: id, data: data});
    data['id'] = id;
    // print({'sql': 'update', 'list': Lists.fromJson(data).toMap()});
    print({'sql': 'update'});

    await db.update(table, data, where: 'id = ?', whereArgs: [id]);
  }

  delete(table, id) async {
    var db = await initDatabase();

    await db.delete(table, where: 'id = ?', whereArgs: [id]);
  }

  closeDatabase() async {
    await this.db.close();
  }
}
